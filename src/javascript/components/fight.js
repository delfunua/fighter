import { controls } from '../../constants/controls';

const acceptedKeys = Object.values(controls).reduce((acc, e) => acc.concat(e), []);
const pressedKeys = new Map();

export async function fight(firstFighter, secondFighter) {
  firstFighter.maxHealth = firstFighter.health;
  secondFighter.maxHealth = secondFighter.health;
 	firstFighter.isComboAvailable = true;
 	secondFighter.isComboAvailable = true;
 	firstFighter.isBlock = false;
 	secondFighter.isBlock = false;
 	firstFighter.position = 'left';
 	secondFighter.position = 'right';
 	firstFighter.currentAction = null;
 	secondFighter.currentAction = null;

 	const keydown = event => {
		if (!acceptedKeys.includes(event.code)) {
			return;
		}
		if (!pressedKeys.get(event.code)) {
			pressedKeys.set(event.code, event.timeStamp);
		}
		attemptCombo(firstFighter, secondFighter, controls.PlayerOneCriticalHitCombination);
		attemptCombo(secondFighter, firstFighter, controls.PlayerTwoCriticalHitCombination);
		switch (event.code) {
			case controls.PlayerOneAttack:
				hit(firstFighter, secondFighter);
				break;
			case controls.PlayerTwoAttack:
				hit(secondFighter, firstFighter);
				break;
			case controls.PlayerOneBlock:
				block(firstFighter);
				break;
			case controls.PlayerTwoBlock:
				block(secondFighter);
				break;
		}
  }

	document.addEventListener('keydown', keydown);

  const keyup = event => {
  	if (!acceptedKeys.includes(event.code)) {
			return;
		}
		if (pressedKeys.get(event.code)) {
			pressedKeys.delete(event.code);
		}
  	switch (event.code) {
  		case controls.PlayerOneBlock:
  			firstFighter.currentAction = null;
  			document.querySelector('.arena___left-fighter').classList.remove('arena___left-fighter-block');
  			break;
  		case controls.PlayerTwoBlock:
  			secondFighter.currentAction = null;
  			document.querySelector('.arena___right-fighter').classList.remove('arena___right-fighter-block');
  			break;
  	}
  }

  document.addEventListener('keyup', keyup);

  return new Promise((resolve) => {
  	let timer = setInterval(() => {
	  	if (firstFighter.health <= 0 || secondFighter.health <= 0)
	  	{
	  		document.removeEventListener('keydown', keydown);
	  		document.removeEventListener('keyup', keyup);
	  		clearInterval(timer);
	  		firstFighter.health <= 0
	  			? document.querySelector('.arena___left-fighter').style.display = 'none'
	  			: document.querySelector('.arena___right-fighter').style.display = 'none';
			  resolve(secondFighter.health <= 0 ? firstFighter : secondFighter);
	  	}
	  }, 100);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let power = getBlockPower(defender) - getHitPower(attacker);
  return power > 0 ? power : 0;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}

export function getCriticalPower(fighter) {
	return 2 * fighter.attack;
}

export function animate(attacker, defender) {
	const element = document.querySelector(`.arena___${attacker.position}-fighter`);
	switch (attacker.currentAction) {
		case 'Hit':
	    element.classList.add(`arena___${attacker.position}-fighter-hit`);
	    setTimeout(() => {
	    	attacker.currentAction = null;
	    	element.classList.remove(`arena___${attacker.position}-fighter-hit`);
	    }, 500);
			break;
		case 'Critical':
	    element.classList.add(`arena___${attacker.position}-fighter-crit`);
	    setTimeout(() => {
	    	attacker.currentAction = null;
	    	element.classList.remove(`arena___${attacker.position}-fighter-crit`);
	    }, 1000);
			break;
		case 'Block':
			element.classList.add(`arena___${attacker.position}-fighter-block`);
			break;
	}
}

export function hit(attacker, defender) {
	if (attacker.currentAction) {
		return;
	}
	let damage = getDamage(attacker, defender);
	if (defender.currentAction == 'Block') {
		damage -= getBlockPower(defender);
	}
	attacker.currentAction = 'Hit';
	animate(attacker);
	defender.health -= damage;
	let percent = defender.health / defender.maxHealth * 100 < 0 ? 0 : defender.health / defender.maxHealth * 100;
	document.getElementById(`${defender.position}-fighter-indicator`).style.width = `${percent}%`;
	console.log(defender.health / defender.maxHealth * 100);
}

export function block(fighter) {
	if (fighter.currentAction) {
		return;
	}
	fighter.currentAction = 'Block';
	animate(fighter);
}

export function attemptCombo(attacker, defender, combination) {
	if (attacker.currentAction) {
		return;
	}
	if (attacker.isComboAvailable && combination.every(e => pressedKeys.get(e))) {
		const isValidCombo = [...pressedKeys]
			.filter(([key, val]) => combination.includes(key))
			.map(val => val[1])
			.every((val, idx, self) => val - Math.min(...self) <= 100);
		if (isValidCombo) {
			attacker.currentAction = 'Critical';
			animate(attacker);
			defender.health -= getCriticalPower(attacker);
			attacker.isComboAvailable = false;
			let percent = defender.health / defender.maxHealth * 100 < 0 ? 0 : defender.health / defender.maxHealth * 100;
			document.getElementById(`${defender.position}-fighter-indicator`).style.width = `${percent}%`;
			setTimeout(() => {attacker.isComboAvailable = true}, 10000);
		}
	}
}