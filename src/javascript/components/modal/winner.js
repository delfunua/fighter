import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
	const title = 'Congratulations!';
	const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
	bodyElement.innerText = `${fighter.name} wins!`;
  showModal({title, bodyElement});
}
