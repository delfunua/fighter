import { createElement } from '../helpers/domHelper';
import { fighterService } from '../services/fightersService';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter)
  {
    let fighterName = createElement({
      tagName: 'span',
      className: 'arena___fighter-name',
    });
    fighterName.innerText = fighter.name;
    let fighterImage = createFighterImage(fighter);
    let fighterStats = createFighterStats(fighter);
    fighterElement.append(fighterName, fighterImage, fighterStats);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const altClassName = fighter.class ?? '';
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: `fighter-preview___img ${altClassName}`,
    attributes,
  });

  return imgElement;
}

export function createFighterStats(fighter) {
  const { attack, defense, health } = fighter;
  const statKeys = ['attack', 'defense', 'health'];
  const stats = Object.fromEntries(Object.entries(fighter).filter(([key]) => statKeys.includes(key)));
  const containerElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___stats'
  });

  let statsElements = Object.keys(stats).map((key) => {
    if (key == 'health')
    {
      stats[key] /= 15;
    }
    const element = createElement({
      tagName: 'div',
      className: `fighter-preview___stats_${key}-bar`,
    });
    element.style = `width: ${stats[key]*20}%`;
    element.innerText = key.toUpperCase();
    return element;
  });

  containerElement.append(...statsElements);
  return containerElement;
}